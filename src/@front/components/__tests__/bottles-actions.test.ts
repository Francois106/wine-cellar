import { BottleActions, fetchBottles } from '../../core/bottle/actions';

describe('Bottle actions', () => {
  it('should create an action to fetch bottles', () => {
    const expectedAction = {
      type: BottleActions.FETCH_PENDING,
    };
    expect(fetchBottles()).toEqual(expectedAction);
  });
});
