import React from 'react';
import { useForm, SubmitHandler } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';

import { postCredentials } from '../../core/user/actions';
import { IAppState } from '../../core';
import { ICredentials } from '@shared';

interface LoginFormProps {
  onChange: () => void;
  onFinish: () => void;
}

const LoginForm: React.FC<LoginFormProps> = ({ onChange, onFinish }) => {
  const dispatch = useDispatch();

  const { handleSubmit, register } = useForm<ICredentials>();
  const credentialsError = useSelector<IAppState, string | null>(
    ({ user }) => user.error,
  );

  const onLogin: SubmitHandler<ICredentials> = (data: ICredentials) => {
    dispatch(postCredentials(data));
    if (!credentialsError) {
      onFinish();
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <div>
        <form onSubmit={handleSubmit(onLogin)} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Adresse mail"
            autoComplete="email"
            autoFocus
            {...register('login')}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Mot de passe"
            type="password"
            id="password"
            autoComplete="current-password"
            {...register('password')}
          />
          <FormControlLabel
            control={<Checkbox value="remember" />}
            label="Se souvenir de moi"
          />
          {credentialsError ? <div>{credentialsError}</div> : null}
          <Button type="submit" fullWidth variant="contained" color="primary">
            Connexion
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Mot de passe oublié?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" onClick={onChange} variant="body2">
                {'Pas de compte? Créer un compte'}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

export default LoginForm;
