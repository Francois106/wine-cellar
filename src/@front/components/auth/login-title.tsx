import React from 'react';

import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

const LoginTitle: React.FC = () => {
  return (
    <div>
      <Avatar>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h3" variant="h5">
        authentifier
      </Typography>
    </div>
  );
};

export default LoginTitle;
