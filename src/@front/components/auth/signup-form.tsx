import React from 'react';
import { Redirect } from 'react-router';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import 'date-fns';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';

import { ICreateUser } from '@shared';
import { postUser } from '../../core/user/actions';
import { IAppState, Routes } from '../../core';
import { IUser } from '@shared';

interface SignupFormProps {
  onChange: () => void;
  onFinish: () => void;
}

const SignUpForm: React.FC<SignupFormProps> = ({ onChange, onFinish }) => {
  const dispatch = useDispatch();

  const user = useSelector<IAppState, IUser | undefined>(
    ({ user }) => user.user,
  );

  const { handleSubmit, register } = useForm<ICreateUser>();
  const onSignup: SubmitHandler<ICreateUser> = data => {
    dispatch(postUser(data));
  };

  if (user) {
    onFinish();
    return <Redirect to={Routes.HOME} />;
  }

  return (
    <Container component="main" maxWidth="xs">
      <div>
        <form onSubmit={handleSubmit(onSignup)}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="fname"
                {...register('firstName')}
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="Prénom"
                autoFocus
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Nom"
                {...register('lastName')}
                autoComplete="lname"
              />
            </Grid>
            <Grid item xs={12}></Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Adresse mail"
                {...register('email')}
                autoComplete="email"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                {...register('password')}
                label="Mot de passe"
                type="password"
                id="password"
                autoComplete="current-password"
              />
            </Grid>
          </Grid>
          <Button type="submit" fullWidth variant="contained" color="primary">
            Enregistrer
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="#" onClick={onChange} variant="body2">
                Déjà un compte? Login
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

export default SignUpForm;
