import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import Box from '@mui/material/Box';
import SortByAlphaIcon from '@mui/icons-material/SortByAlpha';

import { StyledButtonNoBorder } from '../styled/styled-button';
import { IBottleResponse } from '@shared';
import { sortBottles } from '../../core/bottle/actions';

interface SortBottlesProps {
  bottles: IBottleResponse[];
}

const SortBottles: React.FC<SortBottlesProps> = ({ bottles }) => {
  const dispatch = useDispatch();
  const [domainDesc, setDomainDesc] = useState(false);
  const [vintageDesc, setVintageDesc] = useState(false);
  const [quantityDesc, setQuantityDesc] = useState(false);

  const handleSortByQuantity = () => {
    if (quantityDesc) {
      const sortedBottles = bottles.sort(
        (a, b) => b.quantityInStock - a.quantityInStock,
      );
      dispatch(sortBottles(sortedBottles));
    } else {
      const sortedBottles = bottles.sort(
        (a, b) => a.quantityInStock - b.quantityInStock,
      );
      dispatch(sortBottles(sortedBottles));
    }
    setQuantityDesc(!quantityDesc);
  };
  const handleSortByVintage = () => {
    if (vintageDesc) {
      const sortedBottles = bottles.sort((a, b) => b.vintage - a.vintage);
      dispatch(sortBottles(sortedBottles));
    } else {
      const sortedBottles = bottles.sort((a, b) => a.vintage - b.vintage);
      dispatch(sortBottles(sortedBottles));
    }
    setVintageDesc(!vintageDesc);
  };

  const handleSortByDomain = () => {
    if (domainDesc) {
      const sortedBottles = bottles.sort((a, b) =>
        b.domain.localeCompare(a.domain),
      );
      dispatch(sortBottles(sortedBottles));
    } else {
      const sortedBottles = bottles.sort((a, b) =>
        a.domain.localeCompare(b.domain),
      );
      dispatch(sortBottles(sortedBottles));
    }
    setDomainDesc(!domainDesc);
  };

  return (
    <Box margin="auto">
      <StyledButtonNoBorder type="button" onClick={handleSortByDomain}>
        <SortByAlphaIcon />
      </StyledButtonNoBorder>
      <StyledButtonNoBorder type="button" onClick={handleSortByQuantity}>
        Quantité
      </StyledButtonNoBorder>
      <StyledButtonNoBorder type="button" onClick={handleSortByVintage}>
        millésime
      </StyledButtonNoBorder>
    </Box>
  );
};

export default SortBottles;
