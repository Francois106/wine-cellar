import React from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import IconButton from '@mui/material/IconButton';
import { experimentalStyled as styled } from '@mui/material/styles';
import UploadIcon from '@mui/icons-material/Upload';
import Tooltip from '@mui/material/Tooltip';

import { IAppState, Routes } from '../core';
import { uploadBottles } from '../core/bottle/actions';
import MessageCustom from './utils/message-custom';

const ImportExcel: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const err = useSelector<IAppState, string | null>(
    ({ bottles }) => bottles.error,
  );

  const handleCapture = (event: any) => {
    dispatch(uploadBottles(event.target.files[0]));
    if (err) {
      return <MessageCustom level="error" duration={6000} content={err} />;
    }
    history.push(Routes.HOME);
  };

  const Input = styled('input')({
    display: 'none',
  });

  return (
    <Tooltip title="Import Excel">
      <label htmlFor="icon-button-file">
        <Input
          accept="*"
          id="icon-button-file"
          type="file"
          onChange={handleCapture}
        />
        <IconButton
          color="inherit"
          aria-label="upload picture"
          component="span"
          size="large"
        >
          <UploadIcon />
        </IconButton>
      </label>
    </Tooltip>
  );
};

export default ImportExcel;
