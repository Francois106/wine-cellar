import React from 'react';
import LinkRouter from './link-router';

type navLink = {
  label: string;
  to: string;
};

interface ReusableNavigationProps {
  links: navLink[];
}

const ReusableNavigation: React.FC<ReusableNavigationProps> = ({ links }) => {
  return (
    <>
      {links.map(link => (
        <LinkRouter key={link.label} to={link.to}>
          {link.label}
        </LinkRouter>
      ))}
    </>
  );
};

export default ReusableNavigation;
