import React, { useState, useEffect } from 'react';

import Slider from '@mui/material/Slider';
import Typography from '@mui/material/Typography';

export interface RangeSliderProps {
  min: number;
  max: number;
  onChange: (range: number | number[]) => void;
  value?: [number, number];
}

const RangeSlider: React.FC<RangeSliderProps> = ({
  min,
  max,
  onChange,
  value,
}) => {
  const initialValue = value || [min + 2, min + 7];
  const [localValue, setLocalValue] = useState<number | number[]>(initialValue);

  useEffect(() => {
    const newValue = [min + 2, min + 7];

    setLocalValue(newValue);
    onChange(newValue);
  }, [min, onChange]);

  const handleChange = (event: Event, newValue: number | number[]) => {
    setLocalValue(newValue);
    onChange(newValue);
  };

  return (
    <>
      <Typography id="range-slider" gutterBottom>
        Apogée :
      </Typography>
      <Slider
        value={localValue}
        onChange={handleChange}
        valueLabelDisplay="auto"
        min={min}
        max={max}
      />
    </>
  );
};

export default RangeSlider;
