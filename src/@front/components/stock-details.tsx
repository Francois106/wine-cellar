import React from 'react';
import { useDispatch } from 'react-redux';
import { format } from 'date-fns';
import { useHistory } from 'react-router-dom';

import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

import { IStock } from '@shared';
import { cacheStockInState } from '../core/stock/action';

interface StockDetailsProps {
  stock: IStock;
}

const StockDetails: React.FC<StockDetailsProps> = ({ stock }) => {
  const dispatch = useDispatch();

  const shelf: string = stock.position ? stock.position : 'non renseignée';
  const purchaseDate = stock.purchaseDate
    ? format(new Date(stock.purchaseDate), 'dd/MM/yyyy')
    : '(non renseigné)';
  const title: string = 'Etagère ' + shelf + ', achat du ' + purchaseDate;

  const path = '/drink/' + stock.id;
  const history = useHistory();

  const canDrink = stock.remainQuantity ? stock.remainQuantity > 0 : false;

  const handleClick = () => {
    stock.remainQuantity = stock.remainQuantity ? stock.remainQuantity : 0;
    dispatch(cacheStockInState(stock));
    history.push(path);
  };

  return (
    <Container>
      <Typography
        align="center"
        variant="subtitle1"
        gutterBottom
        component="div"
      >
        {title}
      </Typography>
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Fournisseur</TableCell>
              <TableCell align="right">Quantité initiale</TableCell>
              <TableCell align="right">Quantité restante</TableCell>
              <TableCell align="right">Prix</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                {stock.supplier}
              </TableCell>
              <TableCell align="right">{stock.quantity}</TableCell>
              <TableCell align="right">{stock.remainQuantity}</TableCell>
              <TableCell align="right">{stock.price}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <Button
        variant="contained"
        id="drink-button"
        disabled={!canDrink}
        onClick={handleClick}
        fullWidth
      >
        Boire
      </Button>
    </Container>
  );
};

export default StockDetails;
