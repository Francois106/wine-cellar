import styled from 'styled-components';
import Card from '@mui/material/Card';

export const ItemCard = styled(Card)`
  border-radius: var(--radius-border);
  background-color: var(--color-background-light);
  color: var(--color-background-dark);
  &:hover {
    height: 103%;
    border: 3px solid var(--color-background-light);
    box-shadow: -1px 1px 11px 6px var(--color-background-dark);
    -webkit-box-shadow: -1px 1px 11px 6px var(--color-background-dark);
    -moz-box-shadow: -1px 1px 11px 6px var(--color-background-dark);
  }
`;
