import React, { useState } from 'react';

import Alert, { AlertColor } from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';

interface CustomizedMessageCustomProps {
  level: AlertColor;
  content: string;
  duration: number;
}

const MessageCustom: React.FC<CustomizedMessageCustomProps> = ({
  level,
  content,
  duration,
}) => {
  const [open, setOpen] = useState(true);

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  return (
    <Stack spacing={2} sx={{ width: '100%' }}>
      <Snackbar open={open} autoHideDuration={duration} onClose={handleClose}>
        <Alert onClose={handleClose} severity={level} sx={{ width: '100%' }}>
          {content}
        </Alert>
      </Snackbar>
    </Stack>
  );
};

export default MessageCustom;
