import { createStore, Store, applyMiddleware, combineReducers } from 'redux';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import { composeWithDevTools } from 'redux-devtools-extension';

import { AppAction } from './_types/action';
import bottleEpic from './bottle/epic';
import stockEpic from './stock/epic';
import voucherEpic from './voucher/epic';
import userEpic from './user/epic';
import { IBottleState, bottleReducer } from './bottle/reducer';
import { IStockState, stocksReducer } from './stock/reducer';
import { voucherReducer, IVoucherState } from './voucher/reducer';
import { IUserState, userReducer } from './user/reducer';
import BottleApi from './_api/bottleApi';
import { StockApi } from './_api/stockApi';
import VoucherApi from './_api/voucherApi';
import UserApi from './_api/userApi';

export interface IAppState {
  bottles: IBottleState;
  stocks: IStockState;
  vouchers: IVoucherState;
  user: IUserState;
}

export interface IAppDependencies {
  bottleApi: BottleApi;
  stockApi: StockApi;
  voucherApi: VoucherApi;
  userApi: UserApi;
}

export const rootReducer = combineReducers<IAppState>({
  bottles: bottleReducer,
  stocks: stocksReducer,
  vouchers: voucherReducer,
  user: userReducer,
});

export const configureStore = (
  dependencies: IAppDependencies,
): Store<IAppState> => {
  const epicMiddleware = createEpicMiddleware<
    AppAction<any>,
    AppAction<any>,
    IAppState,
    IAppDependencies
  >({ dependencies });

  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(epicMiddleware)),
  );

  const rootEpic = combineEpics<
    AppAction<any>,
    AppAction<any>,
    IAppState,
    IAppDependencies
  >(bottleEpic, stockEpic, voucherEpic, userEpic);

  epicMiddleware.run(rootEpic);

  return store;
};

export * from './_enum/Routes';
