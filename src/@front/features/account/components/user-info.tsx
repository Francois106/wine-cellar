import React from 'react';

import { IUser } from '@shared';
import Typography from '@mui/material/Typography';

interface UserInfoProps {
  user: IUser;
}

const UserInfo: React.FC<UserInfoProps> = ({ user }) => {
  return (
    <>
      <Typography>
        {user.firstName} {user.lastName}
      </Typography>
      <Typography>{user.birthDate}</Typography>
      <Typography>{user.email}</Typography>
    </>
  );
};

export default UserInfo;
