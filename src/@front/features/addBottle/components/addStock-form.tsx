import React, { useState } from 'react';
import { useForm } from 'react-hook-form';

import TextField from '@mui/material/TextField';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

import { IStock } from '@shared';
import StepperButton, { StepperButtonProps } from './stepper-button';

interface CustomizedFormProps {
  onSubmit: (stock: IStock) => void;
  stepperButtonsProps: StepperButtonProps;
  stock?: Partial<IStock>;
}

const today = new Date();

const AddStockForm: React.FC<CustomizedFormProps> = ({
  onSubmit,
  stepperButtonsProps,
  stock,
}) => {
  const { handleSubmit, register, setValue } = useForm<IStock>({
    defaultValues: stock,
  });

  const onRegister = (stock: IStock) => {
    onSubmit(stock);
  };

  const [purchaseDate, setPurchaseDate] = useState<Date>(today);

  const handleChangeDate = (newValue: any) => {
    setValue('purchaseDate', newValue || today);
    setPurchaseDate(newValue || today);
  };

  return (
    <form onSubmit={handleSubmit(onRegister)}>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="quantity"
        label="Quantité"
        autoComplete="quantity"
        autoFocus
        type="number"
        {...register('quantity')}
      />
      <DatePicker
        label="Date d'achat"
        value={purchaseDate}
        {...register('purchaseDate')}
        onChange={handleChangeDate}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="size"
        label="Format"
        autoComplete="size"
        autoFocus
        {...register('size')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="price"
        label="Prix"
        autoComplete="price"
        autoFocus
        type="number"
        {...register('price')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="supplier"
        label="Fournisseur"
        autoComplete="supplier"
        autoFocus
        {...register('supplier')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="position"
        label="Etagère"
        autoComplete="position"
        autoFocus
        {...register('position')}
      />
      <StepperButton {...stepperButtonsProps} canNext={true} />
    </form>
  );
};

export default AddStockForm;
