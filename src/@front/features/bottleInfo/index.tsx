import React from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import CircularProgress from '@mui/material/CircularProgress';

import { IBottleResponse } from '@shared';
import { IAppState } from '../../core';
import { IRouteParams } from '../../core/_types/route-params';
import ArticleInfo from './components/article-info';

const BottleInfoPage: React.FC = () => {
  const { id } = useParams<IRouteParams>();

  const bottleInfo = useSelector<IAppState, IBottleResponse>(
    ({ bottles }) => bottles.list[id],
  );

  return (
    <>
      {!bottleInfo && <CircularProgress />}
      {bottleInfo && <ArticleInfo bottle={bottleInfo} />}
    </>
  );
};

export default BottleInfoPage;
