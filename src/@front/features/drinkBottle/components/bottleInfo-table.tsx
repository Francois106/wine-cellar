import React from 'react';

import CircularProgress from '@mui/material/CircularProgress';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

import { IBottleResponse, IStock } from '@shared';

interface BottleInfoTableProps {
  bottle: IBottleResponse;
  stock: IStock;
}

const BottleInfoTable: React.FC<BottleInfoTableProps> = ({ bottle, stock }) => {
  const title = bottle && (
    <span>
      {bottle.domain} {bottle.vintage}
    </span>
  );
  const from = stock.supplier ? <span>, de chez {stock.supplier}</span> : '';

  if (!stock || !bottle) {
    return <CircularProgress />;
  }

  return (
    <Box sx={{ borderRadius: 3 }} marginBottom="1.5rem">
      <Typography component="h3">Formulaire de sortie</Typography>
      <Typography>
        {title}
        {from}
      </Typography>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Quantité disponible</TableCell>
              <TableCell align="right">Placée en</TableCell>
              <TableCell align="right">Début de plateau</TableCell>
              <TableCell align="right">Fin de plateau</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                {stock.remainQuantity}
              </TableCell>
              <TableCell align="right">{stock.position}</TableCell>
              <TableCell align="right">{bottle.plateauStartYear}</TableCell>
              <TableCell align="right">{bottle.plateauEndYear}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default BottleInfoTable;
