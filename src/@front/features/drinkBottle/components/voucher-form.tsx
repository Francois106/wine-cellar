import React from 'react';
import { useForm } from 'react-hook-form';

import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import ExpandMore from '@mui/icons-material/ExpandMore';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';

import { IVoucher } from '@shared';
import { useVoucher } from '../voucher-context';
import DiningForm from './dining-form';
import DishForm from './dish-form';

interface VoucherFormProps {
  maxQuantity: number;
  handlePostDrink: (voucher: IVoucher) => void;
}

const VoucherForm: React.FC<VoucherFormProps> = ({
  maxQuantity,
  handlePostDrink,
}) => {
  const {
    state: { voucher },
  } = useVoucher();
  const { dispatch } = useVoucher();
  const { register, handleSubmit } = useForm<IVoucher>({
    defaultValues: voucher,
  });

  const onChange = (values: IVoucher) => {
    dispatch({ type: 'update', payload: values });
  };

  const onClick = () => {
    handlePostDrink(voucher);
  };

  return (
    <>
      <form name="voucher-form" onChange={handleSubmit(onChange)}>
        <TextField
          variant="outlined"
          margin="normal"
          defaultValue={1}
          id="quantity"
          label="Quantité"
          autoComplete="quantity"
          InputProps={{ inputProps: { min: 1, max: maxQuantity } }}
          type="number"
          {...register('quantity', { valueAsNumber: true })}
        />
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          id="recipient"
          label="Destinataire"
          autoComplete="recipient"
          type="text"
          {...register('recipient')}
        />
      </form>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMore />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>Associer un évènement</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <DiningForm />
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMore />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>Associer un plat</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <DishForm />
        </AccordionDetails>
      </Accordion>
      <Button variant="contained" fullWidth onClick={onClick}>
        Enregistrer
      </Button>
    </>
  );
};

export default VoucherForm;
