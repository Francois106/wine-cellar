import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

import { IAppState, Routes } from '../../core';
import { IRouteParams } from '../../core/_types/route-params';
import { postVoucher } from '../../core/voucher/action';
import { IBottleResponse, IStock, IVoucher } from '@shared';
import VoucherForm from './components/voucher-form';
import BottleInfoTable from './components/bottleInfo-table';
import { VoucherProvider } from './voucher-context';

const DrinkBottlePage: React.FC = () => {
  const { id } = useParams<IRouteParams>();
  const history = useHistory();
  const dispatch = useDispatch();

  const stock = useSelector<IAppState, IStock>(({ stocks }) => stocks.list[id]);

  const maxQuantity = stock.remainQuantity
    ? stock.remainQuantity
    : stock.quantity;

  const bottles = useSelector<IAppState, IBottleResponse[]>(({ bottles }) =>
    Object.values(bottles.list),
  );
  const bottleArr = bottles.filter(bottle => bottle.id === stock.bottleId);
  const bottle = bottleArr[0];

  const handlePostDrink = (voucher: IVoucher) => {
    voucher.stockId = stock.id;
    dispatch(postVoucher(voucher));
    history.push(Routes.HOME);
  };

  if (!stock || !bottle) {
    return <CircularProgress />;
  }

  return (
    <>
      <BottleInfoTable bottle={bottle} stock={stock} />
      <Box>
        <VoucherProvider>
          <VoucherForm
            maxQuantity={maxQuantity}
            handlePostDrink={handlePostDrink}
          />
        </VoucherProvider>
      </Box>
    </>
  );
};

export default DrinkBottlePage;
