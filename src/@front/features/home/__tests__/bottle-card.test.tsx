/**
 * @jest-environment jsdom
 */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import BottleCard from '../components/bottle-card';
import { bottleMock } from '@front/mocks/bottle-mock';

const onClick = jest.fn();

beforeEach(() => {
  render(<BottleCard bottle={bottleMock} onCardClick={onClick} />);
});

test('loads and display all components', () => {
  expect(screen.getAllByAltText('sample')).toBeDefined();
  expect(screen.getByText('test appellation')).toBeDefined();
  expect(screen.getByText('2010')).toBeDefined();
  expect(screen.getByText('red')).toBeDefined();
  expect(screen.getByText('Test Domain')).toBeDefined();
});

test('call pass bottleId when clicked', () => {
  fireEvent.click(screen.getByText('voir'));
  expect(onClick).toBeCalledWith('testId');
});
