import React from 'react';

import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';
import CircularProgress from '@mui/material/CircularProgress';

const LoadingCard: React.FC = () => {
  return (
    <Stack spacing={1}>
      <Skeleton variant="text" />
      <Skeleton variant="circular" width={40} height={40} />
      <CircularProgress />
      <Skeleton variant="rectangular" width={210} height={118} />
    </Stack>
  );
};

export default LoadingCard;
