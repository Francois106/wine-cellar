import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { UserDTO } from '@model/dto/user.dto';

export const User = createParamDecorator<UserDTO>(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user.data;
  },
);
