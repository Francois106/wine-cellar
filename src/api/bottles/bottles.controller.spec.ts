import { Test, TestingModule } from '@nestjs/testing';
import { createMock, createMockList } from 'ts-auto-mock';
import { JwtService } from '@nestjs/jwt';

import { BottlesController } from './bottles.controller';
import { BottlesService } from './bottles.service';
import { InventoryService } from '@inventory/inventory.service';
import { StockService } from 'api/stock/stock.service';
import { BottleResponseDTO } from '@model/dto/bottleResponse.dto';
import { BottleRequestDTO } from '@model/dto/bottleRequest.dto';
import { UserDTO } from '@model/dto/user.dto';

describe.skip('BottlesController', () => {
  let controller: BottlesController;
  let bottleService: BottlesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BottlesController],
      providers: [
        { provide: BottlesService, useValue: createMock<BottlesService>() },
        { provide: StockService, useValue: createMock<StockService>() },
        { provide: InventoryService, useValue: createMock<InventoryService>() },
        { provide: JwtService, useValue: createMock<JwtService>() },
      ],
    }).compile();

    controller = module.get<BottlesController>(BottlesController);
    bottleService = module.get<BottlesService>(BottlesService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllBottles', () => {
    it('should return an array of BottleResponseDTO', async () => {
      const user = new UserDTO();
      const result = createMockList<BottleResponseDTO>(1);
      jest
        .spyOn(bottleService, 'findAll')
        .mockImplementation(async () => result);
      expect(await controller.getAllBottles(user)).toBe(result);
    });
  });

  describe('getBottle', () => {
    it('should return a BottleResponseDTO', async () => {
      const result = createMock<BottleResponseDTO>();
      jest
        .spyOn(bottleService, 'findOne')
        .mockImplementation(async () => result);
      expect(await controller.getBottle(result.id)).toBe(result);
    });
  });

  describe('addBottle', () => {
    it('should return a BottleResponseDTO', async () => {
      const user = new UserDTO();
      const bottleRequest = createMock<BottleRequestDTO>();
      const result = createMock<BottleResponseDTO>();
      jest
        .spyOn(bottleService, 'createBottle')
        .mockImplementation(async () => result);
      expect(await controller.addBottle(bottleRequest, user)).toBe(result);
    });
  });

  describe('updateBottle', () => {
    it('should return a BottleResponseDTO', async () => {
      const bottleRequest = createMock<BottleRequestDTO>();
      const result = createMock<BottleResponseDTO>();
      jest
        .spyOn(bottleService, 'saveBottle')
        .mockImplementation(async () => result);
      expect(await controller.updateBottle(bottleRequest)).toBe(result);
    });
  });
});
