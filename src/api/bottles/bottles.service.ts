import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { Workbook, Worksheet } from 'exceljs';

import { Bottle } from '@model/entities/bottle.entity';
import { InventoryService } from '@inventory/inventory.service';
import { StockService } from 'api/stock/stock.service';
import { StockDTO } from '@model/dto/stock.dto';
import { BottleRequestDTO } from '@model/dto/bottleRequest.dto';
import { BottleResponseDTO } from '@model/dto/bottleResponse.dto';
import { UserDTO } from '@model/dto/user.dto';
import { Inventory } from '@model/entities/inventory.entity';
import { UploadException } from '@model/enums/uploadException.enum';

@Injectable()
export class BottlesService {
  constructor(
    @InjectRepository(Bottle)
    private readonly bottleRepository: Repository<Bottle>,
    private readonly inventoryService: InventoryService,
    private readonly stockService: StockService,
  ) {}

  public async findAll(user: UserDTO): Promise<BottleResponseDTO[]> {
    const bottles = await getRepository(Bottle)
      .createQueryBuilder('bottle')
      .innerJoinAndSelect(
        'bottle.stocks',
        'stock',
        'stock.bottleId = bottle.id',
      )
      .innerJoin(Inventory, 'inv', 'stock.inventoryId = inv.id')
      .where('inv.id = :id', { id: user.inventoryId })
      .andWhere('stock.remainQuantity > 0')
      .orderBy('domain')
      .getMany();

    return bottles.map(bottle => {
      this.setQuantityInStock(bottle);
      const bottleDTO = BottleResponseDTO.fromEntity(bottle);
      bottleDTO.stocks = bottleDTO.stocks.map(stock => {
        stock.bottleId = bottleDTO.id;
        return stock;
      });
      return bottleDTO;
    });
  }

  private setQuantityInStock(bottle: Bottle): Bottle {
    bottle.quantityInStock = bottle.stocks.reduce((sum, stock) => {
      return sum + stock.remainQuantity;
    }, 0);
    return bottle;
  }

  public async findOne(id: string): Promise<BottleResponseDTO> {
    const bottle = await this.bottleRepository.findOne(id);
    return BottleResponseDTO.fromEntity(this.setQuantityInStock(bottle));
  }

  public async createBottle(
    bottleDTO: BottleRequestDTO,
    user: UserDTO,
  ): Promise<BottleResponseDTO> {
    if (!bottleDTO.stock.purchaseDate) {
      bottleDTO.stock.purchaseDate = new Date();
    }
    const inventory = await this.inventoryService.findOne(user.inventoryId);
    const bottle = this.toEntityCreateBottle(bottleDTO, inventory);
    const savedBottle = await this.bottleRepository.save(bottle);
    return BottleResponseDTO.fromEntity(savedBottle);
  }

  public async saveBottle(
    bottleDTO: BottleRequestDTO,
  ): Promise<BottleResponseDTO> {
    const bottle = Object.assign(new Bottle(), bottleDTO);
    bottle.stocks = await this.stockService.findByBottle(bottle);
    await this.bottleRepository.save(bottle);
    return BottleResponseDTO.fromEntity(bottle);
  }

  public async deleteBottle(id: string) {
    return this.bottleRepository.delete(id);
  }

  private toEntityCreateBottle(
    dto: BottleRequestDTO,
    inventory: Inventory,
  ): Bottle {
    const bottle = Object.assign(new Bottle(), dto);
    bottle.stocks = [];
    if (dto.stock) {
      const stock = StockDTO.toEntityStock(dto.stock);
      stock.inventory = inventory;
      stock.remainQuantity = stock.quantity;
      bottle.stocks.push(stock);
    }
    return bottle;
  }

  public async uploadFile(sheet: Buffer, user: UserDTO) {
    try {
      const workbook = new Workbook();
      await workbook.xlsx.load(sheet);
      const data = workbook.worksheets[0];
      if (workbook.worksheets.length === 0) {
        throw new InternalServerErrorException(
          UploadException.FILE_SHEET_ERROR,
        );
      }

      const bottles = this.readSheet(data);
      const savedBottles = await this.createBottlesByBatch(bottles, user);

      return savedBottles;
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  private readSheet(data: Worksheet): any[] {
    const bottles = [];
    data.eachRow((row, rowNumber) => {
      if (rowNumber > 1) {
        try {
          const bottle = BottleRequestDTO.fromExcelRow(row);
          bottles.push(bottle);
        } catch (error) {
          throw new InternalServerErrorException(
            UploadException.FILE_ROW_ERROR + rowNumber,
          );
        }
      }
    });
    return bottles;
  }

  private async createBottlesByBatch(
    bottleDTOs: BottleRequestDTO[],
    user: UserDTO,
  ): Promise<BottleResponseDTO[]> {
    const inventory = await this.inventoryService.findOne(user.inventoryId);
    return Promise.all(
      bottleDTOs.map(async bottleDTO => {
        const bottle = this.toEntityCreateBottle(bottleDTO, inventory);
        const savedBottle = await this.bottleRepository.save(bottle);
        return BottleResponseDTO.fromEntity(savedBottle);
      }),
    );
  }

  public async allBottlesToExcelFile(user: UserDTO): Promise<Workbook> {
    const bottlesDto = await this.findAll(user);

    const workbook = new Workbook();
    workbook.modified = new Date();
    const sheet = workbook.addWorksheet('Mes Vins', {
      pageSetup: { paperSize: 9, orientation: 'landscape' },
    });
    sheet.columns = [
      { header: 'Couleur', key: 'color' },
      { header: 'Quantité', key: 'quantity' },
      { header: 'Etagère', key: 'position' },
      { header: 'Région', key: 'region' },
      { header: 'Appellation', key: 'appellation' },
      { header: 'Domain', key: 'domain' },
      { header: 'Cuvée', key: 'label' },
      { header: 'Vintage', key: 'vintage' },
      { header: "Date d'achat", key: 'purchaseDate' },
      { header: 'Fournisseur', key: 'supplier' },
      { header: 'Prix unitaire', key: 'price' },
      { header: 'Boire à partir de', key: 'readyYear' },
      { header: 'Apogée', key: 'plateauRangeYear' },
      { header: 'Boire avant', key: 'expirationYear' },
      { header: 'Commentaire', key: 'comment' },
    ];
    sheet.getRow(1).font = { bold: true };
    sheet.getRow(1).alignment = {
      vertical: 'middle',
      horizontal: 'center',
      wrapText: true,
    };
    sheet.getRow(1).border = {
      top: { style: 'thin' },
      left: { style: 'thin' },
      bottom: { style: 'thin' },
      right: { style: 'thin' },
    };
    for (let i = 0; i < bottlesDto.length; i++) {
      const bottleDto = bottlesDto[i];
      try {
        const stocks = await this.stockService.findStocksByBottleId(
          bottleDto.id,
        );
        this.addBottleToSheet(sheet, bottleDto, stocks);
      } catch (err) {
        throw new Error(err.message);
      }
    }

    return workbook;
  }

  private addBottleToSheet(
    sheet: Worksheet,
    bottleDto: BottleResponseDTO,
    stocks: StockDTO[],
  ) {
    sheet.addRow({
      color: bottleDto.color,
      quantity: bottleDto.quantityInStock,
      position: stocks[0].position,
      region: bottleDto.region,
      appellation: bottleDto.appellation,
      domain: bottleDto.domain,
      label: bottleDto.label,
      vintage: bottleDto.vintage,
      purchaseDate: stocks[0]?.purchaseDate,
      supplier: stocks[0]?.supplier,
      price: stocks[0]?.price,
      readyYear: bottleDto.readyYear,
      plateauRangeYear: `${bottleDto.plateauStartYear} - ${bottleDto.plateauEndYear}`,
      expirationYear: bottleDto.expirationYear,
      comment: bottleDto.comment,
    });
  }
}
