import { Body, Controller, Get, Param, Post } from '@nestjs/common';

import { Inventory } from '@model/entities/inventory.entity';
import { InventoryService } from './inventory.service';

@Controller('inventory')
export class InventoryController {
  constructor(private readonly inventoryService: InventoryService) {}

  @Get()
  getAllInventories(): Promise<Inventory[]> {
    return this.inventoryService.findAll();
  }

  @Get(':id')
  getInventory(@Param('id') id: string): Promise<Inventory> {
    return this.inventoryService.findOne(id);
  }

  @Post()
  addInventory(@Body() inventory: Inventory): Promise<Inventory> {
    return this.inventoryService.saveInventory(inventory);
  }
}
