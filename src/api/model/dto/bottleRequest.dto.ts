import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Row } from 'exceljs';

import { IBottleRequest } from '@shared';
import { Bottle } from '@model/entities/bottle.entity';
import { Photo } from '@model/entities/photo.entity';
import { StockDTO } from './stock.dto';

export class BottleRequestDTO implements IBottleRequest {
  @ApiProperty()
  id: string;

  @ApiProperty()
  @IsString()
  domain: string;

  @ApiProperty()
  color: string;

  @ApiProperty()
  region: string;

  @ApiProperty()
  appellation: string;

  @ApiProperty()
  vintage: number;

  @ApiProperty()
  label: string;

  @ApiProperty()
  readyYear: number;

  @ApiProperty()
  plateauStartYear: number;

  @ApiProperty()
  plateauEndYear: number;

  @ApiProperty()
  expirationYear: number;

  @ApiProperty()
  expirationString?: string;

  @ApiProperty()
  comment: string;

  @ApiProperty()
  photos: Photo[];

  @ApiProperty()
  stock: StockDTO;

  public static from(dto: Partial<BottleRequestDTO>) {
    return Object.assign(new BottleRequestDTO(), dto);
  }

  public static fromEntity(entity: Bottle) {
    return this.from({
      id: entity.id,
      domain: entity.domain,
      color: entity.color,
      region: entity.region,
      appellation: entity.appellation,
      vintage: entity.vintage,
      label: entity.label,
      readyYear: entity.readyYear,
      plateauStartYear: entity.plateauStartYear,
      plateauEndYear: entity.plateauEndYear,
      expirationYear: entity.expirationYear,
      expirationString: entity.expirationString,
      comment: entity.comment,
      photos: entity.photos,
      stock: entity.stocks ? StockDTO.fromEntity(entity.stocks[0]) : null,
    });
  }

  public static fromExcelRow(row: Row): BottleRequestDTO {
    const bottle = new BottleRequestDTO();
    bottle.domain = row.getCell('I').text;
    bottle.region = row.getCell('G').text;
    bottle.appellation = row.getCell('H').text;
    bottle.color = row.getCell('B').text;
    bottle.comment = row.getCell('V').text;
    bottle.label = row.getCell('J').text;
    bottle.vintage = row.getCell('K').value
      ? parseInt(row.getCell('K').text)
      : 0;

    bottle.readyYear = row.getCell('R').value
      ? parseInt(row.getCell('R').text)
      : 0;
    if (row.getCell('S').value && row.getCell('S').text.length == 4) {
      bottle.plateauStartYear = parseInt(row.getCell('S').text);
      bottle.plateauEndYear = parseInt(row.getCell('S').text);
    } else if (row.getCell('S').value && row.getCell('S').text.length == 9) {
      bottle.plateauStartYear = parseInt(row.getCell('S').text.split('-')[0]);
      bottle.plateauEndYear = parseInt(row.getCell('S').text.split('-')[1]);
    } else if (row.getCell('S').value && row.getCell('S').text.length > 3) {
      bottle.plateauStartYear = parseInt(row.getCell('S').text.slice(0, 4));
    }
    bottle.expirationString = row.getCell('U').text;

    const stock = new StockDTO();
    const dateString = row.getCell('L').text;
    if (dateString.length == 8) {
      const year = parseInt(dateString.slice(0, 4));
      const month = parseInt(dateString.slice(4, 6));
      const day = parseInt(dateString.slice(6));
      stock.purchaseDate = new Date(year, month, day);
    } else {
      stock.purchaseDate = new Date();
    }
    stock.quantity = row.getCell('N').value
      ? parseInt(row.getCell('N').text)
      : 0;
    stock.remainQuantity = stock.quantity;
    stock.price = row.getCell('O').value ? parseInt(row.getCell('O').text) : 0;
    stock.position = row.getCell('E').text;
    stock.size = row.getCell('D').text;
    stock.supplier = row.getCell('M').text;

    bottle.stock = stock;

    return bottle;
  }
}
