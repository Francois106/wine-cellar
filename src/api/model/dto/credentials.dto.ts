import { ApiProperty } from '@nestjs/swagger';

import { ICredentials } from '@shared';

export class CredentialsDTO implements ICredentials {
  @ApiProperty()
  login: string;
  @ApiProperty()
  password: string;
}
