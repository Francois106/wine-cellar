import { ApiProperty } from '@nestjs/swagger';

import { Dining } from '@model/entities/dining.entity';
import { IDining } from '@shared';

export class DiningDTO implements IDining {
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  location: string;

  public static from(dto: Partial<DiningDTO>) {
    return Object.assign(new DiningDTO(), dto);
  }

  public static fromEntity(entity: Dining) {
    return this.from({
      id: entity.id,
      name: entity.name,
      location: entity.location,
    });
  }
}
