import { ApiProperty } from '@nestjs/swagger';

import { User } from '@model/entities/user.entity';
import { IsString } from 'class-validator';
import { IUser } from '@shared';

export class UserDTO implements IUser {
  @ApiProperty()
  id: string;

  @ApiProperty()
  @IsString()
  email: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  birthDate: Date;

  @ApiProperty()
  photosUrl: string[];

  @ApiProperty()
  createDateTime: Date;

  @ApiProperty()
  expirationDate: Date;

  @ApiProperty()
  inventoryId: string;

  public static from(dto: Partial<UserDTO>): UserDTO {
    return Object.assign(new UserDTO(), dto);
  }

  public static fromEntity(entity: User): UserDTO {
    return this.from({
      id: entity.id,
      email: entity.credentials.login,
      firstName: entity.firstName,
      lastName: entity.lastName,
      birthDate: entity.birthDate,
      photosUrl: entity.photos ? entity.photos.map(photo => photo.url) : [],
      createDateTime: entity.createDateTime,
      expirationDate: entity.expirationDate,
      inventoryId: entity.inventory.id,
    });
  }
}
