import type { IDining, IDish, IVoucher } from '@shared';

import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

import { Voucher } from '@model/entities/voucher.entity';

export class VoucherDTO implements IVoucher {
  @ApiProperty()
  id: string;

  @ApiProperty()
  @IsNumber()
  quantity: number;

  @ApiProperty()
  @IsString()
  stockId: string;

  @ApiProperty()
  createdDateTime: Date;

  @ApiProperty()
  recipient: string;

  @ApiProperty()
  dining: IDining;

  @ApiProperty()
  dish: IDish;

  public static from(dto: Partial<VoucherDTO>) {
    return Object.assign(new VoucherDTO(), dto);
  }

  public static fromEntity(entity: Voucher) {
    return this.from({
      id: entity.id,
      quantity: entity.quantity,
      stockId: entity.stock.id,
      createdDateTime: entity.createdDateTime,
      recipient: entity.recipient,
    });
  }
}
