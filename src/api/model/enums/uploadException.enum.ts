export enum UploadException {
  'FILE_ROW_ERROR' = 'An error occured during upload at row ',
  'FILE_SHEET_ERROR' = "Can't find worksheet",
}
