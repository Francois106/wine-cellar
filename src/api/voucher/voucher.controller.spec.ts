import { Test, TestingModule } from '@nestjs/testing';
import { createMock } from 'ts-auto-mock';
import { JwtService } from '@nestjs/jwt';

import { VoucherDTO } from '@model/dto/voucher.dto';
import { VoucherController } from './voucher.controller';
import { VoucherService } from './voucher.service';

describe.skip('VoucherController', () => {
  let controller: VoucherController;
  let diningService: VoucherService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VoucherController],
      providers: [
        { provide: VoucherService, useValue: createMock<VoucherService>() },
        { provide: JwtService, useValue: createMock<JwtService>() },
      ],
    }).compile();

    controller = module.get<VoucherController>(VoucherController);
    diningService = module.get<VoucherService>(VoucherService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('addDining', () => {
    it('should return a VoucherDTO', async () => {
      const result = createMock<VoucherDTO>();
      jest
        .spyOn(diningService, 'createVoucher')
        .mockImplementation(async () => result);
      expect(await controller.addVoucher(new VoucherDTO())).toBe(result);
    });
  });
});
