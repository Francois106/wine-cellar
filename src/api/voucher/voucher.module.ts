import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { Dining } from '@model/entities/dining.entity';
import { Dish } from '@model/entities/dish.entity';
import { Stock } from '@model/entities/stock.entity';
import { Voucher } from '@model/entities/voucher.entity';
import { VoucherController } from './voucher.controller';
import { VoucherService } from './voucher.service';
import { AuthModule } from '@auth/auth.module';
import { EnvironmentVariables } from '@model/enums/environment.enum';

@Module({
  imports: [
    TypeOrmModule.forFeature([Dining, Voucher, Stock, Dish]),
    AuthModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>(EnvironmentVariables.SECRET),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [VoucherService],
  controllers: [VoucherController],
})
export class VoucherModule {}
