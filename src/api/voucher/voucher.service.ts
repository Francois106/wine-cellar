import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository } from 'typeorm';

import { Voucher } from '@model/entities/voucher.entity';
import { VoucherDTO } from '@model/dto/voucher.dto';
import { VoucherResponseDTO } from '@model/dto/voucherResponse.dto';
import { Stock } from '@model/entities/stock.entity';
import { Bottle } from '@model/entities/bottle.entity';

@Injectable()
export class VoucherService {
  constructor(
    @InjectRepository(Voucher)
    private readonly voucherRepository: Repository<Voucher>,
    @InjectRepository(Stock)
    private readonly stockRepository: Repository<Stock>,
  ) {}

  public async createVoucher(voucherDTO: VoucherDTO): Promise<VoucherDTO> {
    const stock = await this.stockRepository.findOne(voucherDTO.stockId);
    stock.remainQuantity -= voucherDTO.quantity;
    const voucher = this.toEntityVoucher(voucherDTO);
    voucher.stock = stock;
    const savedVoucher = await this.voucherRepository.save(voucher);
    return VoucherDTO.fromEntity(savedVoucher);
  }

  private toEntityVoucher(dto: VoucherDTO): Voucher {
    return Object.assign(new Voucher(), dto);
  }

  public async findAll(): Promise<VoucherResponseDTO[]> {
    const vouchers = await getRepository(Voucher)
      .createQueryBuilder('voucher')
      .innerJoinAndSelect(
        'voucher.stock',
        'stock',
        'voucher.stockId = stock.id',
      )
      .innerJoinAndSelect(
        'voucher.dining',
        'dining',
        'voucher.diningId = dining.id',
      )
      .innerJoinAndSelect('voucher.dish', 'dish', 'voucher.dishId = dish.id')
      .innerJoin(Bottle, 'bottle', 'stock.bottleId = bottle.id')
      .addSelect('bottle.domain', 'bottleDomain')
      .addSelect('bottle.id', 'bottleId')
      .orderBy({ 'voucher.createdDateTime': 'DESC' })
      .getRawMany();
    return vouchers.map(voucher => VoucherResponseDTO.fromRowdata(voucher));
  }
}
