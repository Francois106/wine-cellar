import { Controller, Get, Render } from '@nestjs/common';

@Controller()
export class AppControler {
  @Get('*')
  @Render('app')
  renderApp() {
    return {};
  }
}
