/* eslint-disable @next/next/google-font-display */
import React from 'react';
import Document, { Head, Html, Main, NextScript } from 'next/document';

export interface IAppDocumentProps {
  lang: string;
}

export default class AppDocument extends Document<IAppDocumentProps> {
  render() {
    return (
      <Html lang={this.props.lang}>
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <link
            rel="apple-touch-icon"
            href="/_next/image?url=%2Fpublic%2Flogo192.png&w=3840&q=75"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
          />
        </Head>
        <body>
          <noscript>You need to enable JavaScript to run this app.</noscript>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

AppDocument.getInitialProps = async ctx => {
  const initialProps = await Document.getInitialProps(ctx);

  return {
    ...initialProps,
    lang: ctx.query.lang || ctx.locale,
  };
};
