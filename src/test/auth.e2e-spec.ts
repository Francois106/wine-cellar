import request from 'supertest';
import { Repository } from 'typeorm';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { Credentials } from '@model/entities/credentials.entity';
import { User } from '@model/entities/user.entity';
import userFixture from '@model/fixtures/user-fixture.json';
import { MainModule } from '../main.module';
import { UserCreateDTO } from '@model/dto/userCreate.dto';
import { Inventory } from '@model/entities/inventory.entity';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let credentialsRepository: Repository<Credentials>;
  let inventoryRepository: Repository<Inventory>;
  let userRepository: Repository<User>;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [MainModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    userRepository = app.get('UserRepository');
    credentialsRepository = app.get('CredentialsRepository');
    inventoryRepository = app.get('InventoryRepository');

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('Auth module', () => {
    let user: User;
    let inventory: Inventory;

    beforeEach(async () => {
      try {
        user = await userRepository.save(userFixture[1]);
        inventory = await inventoryRepository.save(new Inventory());
        user.inventory = inventory;
        await userRepository.save(user);
      } catch (err) {
        throw new Error(err);
      }
    });

    afterEach(async () => {
      try {
        await credentialsRepository.query(
          'TRUNCATE TABLE credentials CASCADE;',
        );
        await inventoryRepository.query('TRUNCATE TABLE inventory CASCADE;');
      } catch (err) {
        throw new Error(err);
      }
    });

    describe('when POST /auth/login', () => {
      it('should return a userTokenDTO', async () => {
        const res = await request(app.getHttpServer())
          .post('/auth/login')
          .set('Accept', 'application/json')
          .send({ login: 'testAuth', password: 'pwd' })
          .expect(200);
        expect(res.body.user.id).toBeDefined();
      });
    });

    describe('when POST /auth/register', () => {
      it('should save the user in base and return a userTokenDTO', async () => {
        const userCreateDTO = new UserCreateDTO();
        userCreateDTO.email = '@email';
        userCreateDTO.password = 'pwd';
        const res = await request(app.getHttpServer())
          .post('/auth/register')
          .set('Accept', 'application/json')
          .send(userCreateDTO)
          .expect(201);
        expect(res.body.user.id).toBeDefined();
      });
    });

    describe('when GET /auth/me', () => {
      it('should return a userDTO', async () => {
        const data = await request(app.getHttpServer())
          .post('/auth/login')
          .set('Accept', 'application/json')
          .send({ login: 'testAuth', password: 'pwd' });
        const token = data.body.token;
        const res = await request(app.getHttpServer())
          .get('/auth/me')
          .set('Authorization', 'Bearer ' + token)
          .expect(200);
        expect(res.body.id).toBeDefined();
        expect(res.body.email).toBe('testAuth');
      });
    });
  });
});
